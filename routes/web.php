<?php
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/grade', function () {
    return view('grade.create');
});
Route::post('/process', function (Request $request) {
    $mark = $request->mark;
    $name = $request->title;
    if (strlen($mark) > 0) {
        if (0 > $mark or $mark > 100) {
            return redirect('/grade')->with('ErrorMsg', 'Opps! Invalid Input. Please enter mark between 0 - 100');
        }
        if ($mark < 40  ) {
            return view('grade.mypage', ['myname' => $name, 'result' => 'F']);
        }

        if ( $mark >=40  && $mark <= 49) {
            return view('grade.mypage', ['myname' => $name, 'result' => 'D']);
        }

        if ($mark >=50 && $mark <= 59) {
            return view('grade.mypage', ['myname' => $name, 'result' => 'C']);
        }

        if ($mark >=60 && $mark <= 69) {
            return view('grade.mypage', ['myname' => $name, 'result' => 'B']);
        }

        if ($mark >=70 && $mark <= 79) {
            return view('grade.mypage', ['myname' => $name, 'result' => 'A']);
        }
        if ($mark >=80 && $mark <= 89) {
            return view('grade.mypage', ['myname' => $name, 'result' => 'A+']);
        }

        if ($mark >=90 && $mark <= 100) {
            return view('grade.mypage', ['myname' => $name, 'result' => 'Golden A+']);
        }
    } else {
        return redirect('/grade')->with('ErrorMsg', 'Mark can not be empty');
    }
});